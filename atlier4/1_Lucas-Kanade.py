import numpy as np
import cv2

#Créer un objet VideoCapture et lire à partir du fichier d'entrée
# Si l'entrée est la caméra, transmettez 0 à la place du nom du fichier vidéo
cap = cv2.VideoCapture('C:\\Users\\othman\\Desktop\\ateliers\\vtest.avi')

# Créer des couleurs aléatoires
color = np.random.randint(low=0,high=255,size=(100,3))

# Prendre le premier cadre et y trouver des coins
ret, frameS = cap.read()

# régler la saturation c à d convertir les frames en nuveau gris
grisS = cv2.cvtColor(frameS, cv2.COLOR_BGR2GRAY)

# definie les parametres suivants ; paramètres pour la détection de coins ShiTomasi
parms = dict( maxCorners = 100,qualityLevel = 0.3,minDistance = 7,blockSize = 7 )

#. Déterminer les angles forts d'une image.
#. minDistance : Distance euclidienne minimale possible entre les angles renvoyés
#. mask : Région d'intérêt optionnell. Si l'image n'est pas vide (elle doit avoir le type CV_8UC1 et la même taille que l'image), il spécifie la région 
# dans laquelle les coins sont détectés.
#. blockSize : Taille d'un bloc moyen pour calculer une matrice de covariance dérivée sur chaque voisinage de pixel
#. qualityLevel : Paramètre caractérisant la qualité minimale acceptée des coins d'image. La valeur du paramètre est multipliée par la meilleure mesure 
# de la qualité des angles, qui est la valeur propre minimale.
# ou la réponse de la fonction Harris .les angles avec la mesure de qualité inférieure au produit sont rejetés.
# Par exemple, si le meilleur coin a la mesure de qualité = 1500 et le niveau de qualité = 0,01, tous les coins avec une mesure de qualité inférieure à 15 sont rejetés.
#. maxCorners : Nombre maximum de coins à retourner. S'il y a plus de coins que ceux trouvés, le plus fort d'entre eux est renvoyé
angles = cv2.goodFeaturesToTrack(grisS, mask = None, **parms)
print(angles)

# Créer une image de masque à des fins de dessin
masque = np.zeros_like(frameS)

# definie les parametres suivants ; Paramètres pour le flux optique lucas kanade
paramsLK = dict( winSize  = (15,15),maxLevel =2 ,criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

# Vérifier si la caméra s'est ouverte avec succès
if (cap.isOpened()== False): 
    print("Error opening video stream or file")

else:
    while(1):
         # capturer frame_par_frame
         ret,frame = cap.read()
         #casser la boucle
         if ret == False:
             break
         # régler la saturation c à d convertir les frames en nuveau gris
         frameGris = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

         #. calculer le flux optique
         #. winSize : taille de la fenêtre de recherche à chaque niveau de pyramide.
         #. criteria : paramètre, spécifiant les critères de terminaison de l'algorithme de recherche itérative (après le nombre maximal spécifié 
         # d'itérations criteria.maxCount(cv2.TERM_CRITERIA_COUNT) ou lorsque la fenêtre de recherche se déplace de moins que criteria.epsilon(cv2.TERM_CRITERIA_EPS).
         #. maxLevel : Nombre maximal de niveaux de pyramide basé sur 0; si elles sont définies sur 0, les pyramides ne sont pas utilisées (niveau unique), si elles 
         # sont définies sur 1, deux niveaux sont utilisés, etc. 
         # si les pyramides sont passées en entrée, l'algorithme utilisera autant de niveaux que de pyramides mais pas plus que maxLevel.
         #. err : vecteur de sortie des erreurs; chaque élément du vecteur est défini sur une erreur pour la fonction correspondante; le type de mesure d'erreur 
         # peut être défini dans le paramètre flags; si le flux n'a pas été trouvé, l'erreur n'est pas définie
         #. status : vecteur d'état de sortie (de caractères non signés); chaque élément du vecteur est mis à 1 si le flux 
         # pour les entités correspondantes a été trouvé, sinon, il est mis à 0.
         #. pointNext : vecteur de sortie de points 2D (avec des coordonnées à virgule flottante simple précision) contenant les nouvelles positions calculées
         # des entités en entrée dans la deuxième image; Lorsque 
         # le drapeau OPTFLOW_USE_INITIAL_FLOW est passé, le vecteur doit avoir la même taille que dans l'entrée.
         pointNext, status, err = cv2.calcOpticalFlowPyrLK(grisS, frameGris , angles, None, **paramsLK)

         # Sélectionnez les bons points
         points_new = pointNext[status==1]
         points_old = angles[status==1]

         # dessiner les pistes
         for i,(point_new,point_old) in enumerate(zip(points_new,points_old)):
             # convertir des tableau en tableau 1D
             newA,newB = point_new.ravel()
             oldA,oldB = point_old.ravel()
             #tracer ligne
             masque = cv2.line(masque, (newA,newB),(oldA,oldB), color[i].tolist(), 2)
             #tracer circle
             frame = cv2.circle(frame,(newA,newB),5,color[i].tolist(),-1)
             
         # addition de deux frames
         frameLK = cv2.add(frame,masque)
         if ret == True:
             # afficher le cadre obtenu
             cv2.imshow('Frame_lukas_kanake',frameLK)
             #cv2.imshow('Frame',frame)
             # appuyez sur e sur le clavier pour quitter
             if cv2.waitKey(25) & 0xFF == ord('e'):
                 break

         # Maintenant, mettez à jour le cadre précédent et les points précédents
         grisS = frameGris.copy()
         #matrice avec un seul ligne et 2 colonnes
         angles = points_new.reshape(-1,1,2)
         
    #Lorsque tout est terminé, libérez l'objet de capture vidéo
    cap.release()
    # Ferme tous les cadres
    cv2.destroyAllWindows()
