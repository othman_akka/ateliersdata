import cv2
import numpy as np


#Créer un objet VideoCapture et lire à partir du fichier d'entrée
# Si l'entrée est la caméra, transmettez 0 à la place du nom du fichier vidéo
cap = cv2.VideoCapture('C:\\Users\\othman\\Desktop\\ateliers\\vtest.avi')

# capturer image 1
ret, frame1 = cap.read()

# premiere frame en niveau gris
frame = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)

#creer frame avec taille de frame1
image = np.zeros_like(frame1) 

# régler la saturation de couche 2 d'image hsv (les couches 0 1 2)
# couche 1 <-- 255 parceque lorsque utilise la couche 0 ou 2 avec valeur 255 ,les objets devenent noir blanc 
image[...,1] = 255

# Vérifier si la caméra s'est ouverte avec succès
if (cap.isOpened()== False): 
    print("Error opening video stream or file")

else:
    # capturer frame_par_frame
    ret, frame2 = cap.read()
         
    # régler la saturation c à d convertir les frames en nuveau gris
    frame3 = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
         
    # obtenir des paramètres de flux optique dense
    # flow a deux couche 0 et 1
    # pyr_scale=0.5 :signifie une pyramide classique, où chaque couche suivante est deux fois plus petite que la précédente.
    # levels=1 : signifie qu'aucune couche supplémentaire n'est créée et que seules les images d'origine sont utilisées
    # winsize=15 : Des valeurs plus grandes augmentent la robustesse de l'algorithme vis-à-vis du bruit d'image et donnent plus de chances de détection de mouvement rapide, mais produisent un champ de mouvement plus flou.
    # iterations=2 : nombre d'itérations que l'algorithme fait à chaque niveau de pyramide.
    # poly_n=5 : taille du voisinage de pixel utilisé pour trouver une expansion polynomiale dans chaque pixel; des valeurs plus grandes signifient que l'image sera approximée avec des surfaces plus lisses, ce qui donnera un algorithme plus robuste et un champ de mouvement plus flou, typiquement poly_n = 5 ou 7.
    #oly_sigma=1.1 : standard deviation of the Gaussian that is used to smooth derivatives used as a basis for the polynomial expansion; for poly_n=5, you can set poly_sigma=1.1, for poly_n=7, a good value would be poly_sigma=1.5.
    flow = cv2.calcOpticalFlowFarneback(frame,frame3, flow=None,pyr_scale=0.5, levels=1, winsize=15,iterations=2,poly_n=5, poly_sigma=1.2, flags=0)
         
    # convertir de cartésien à polaire
    # Calcule la magnitude et l'angle des vecteurs 2D
    # mag et ang sont deux frame 
    # magnitude(I)=sqrt(flow[...,0]^2+flow[...,1]^2)
    # angle=atan2(flow[...,1]/flow[...,0])(180/pi)
    magnitude, angle = cv2.cartToPolar(flow[...,0], flow[...,1])
   
    # la teinte correspond à la direction
    # pi/2 pour ameliorer le resultat
    image[...,0] = angle*180/np.pi/2
         
    # la valeur correspond à la magnitude
    # Normalise la norme ou la plage de valeurs d'un tableau
    # min(hsv[...,2])=0 et max(hsv[...,2])=255 ;cv2.NORM_MINMAX est un masque
    image[...,2] = cv2.normalize(magnitude,None,0,255,cv2.NORM_MINMAX)
         
    # régler la saturation
    frame_rgb = cv2.cvtColor(image,cv2.COLOR_HSV2BGR)
    
    # Afficher le cadre obtenu
    cv2.imshow('Dense',frame_rgb)
    cv2.imshow('Frame ',frame2)
    cv2.imwrite('Dense.jpg',frame_rgb)
#Lorsque tout est terminé, libérez l'objet de capture vidéo
cap.release()
# Ferme tous les cadres
cv2.destroyAllWindows()
   

