import cv2

#Créer un objet VideoCapture et lire à partir du fichier d'entrée
# Si l'entrée est la caméra, transmettez 0 à la place du nom du fichier vidéo
#cap = cv2.VideoCapture('C:\\Users\\othman\\Desktop\\ateliers\\vtest.avi')
cap = cv2.VideoCapture(0)
# Vérifier si la caméra s'est ouverte avec succès
if (cap.isOpened()== False): 
    print("Error opening video stream or file")
else:
    i=0
    while(cap.isOpened()):
        # Capturer frame-par-frame
        ret, frame = cap.read();
        #casser la boucle
        if ret == False:
            break
        if ret == True:
            # Afficher le cadre obtenu 
            cv2.imshow('Frame',frame)
            i=i+1
            cv2.imwrite("C:\\Users\\othman\\Desktop\\ateliers\\atelier5\\frames\\image"+str(i)+".jpg",frame)
            # Appuyez sur e sur le clavier pour quitter
            if cv2.waitKey(25) & 0xFF == ord('e'):
                break
   # Lorsque tout est terminé, libérez l'objet de capture vidéo 
    cap.release()
   # Ferme tous les cadres par exemple lorsque clique sur e
    cv2.destroyAllWindows()
  

