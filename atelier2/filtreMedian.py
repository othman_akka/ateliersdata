from PIL import Image
import cv2
import numpy as np

#Créer un objet VideoCapture et lire à partir du fichier d'entrée
# Si l'entrée est la caméra, transmettez 0 à la place du nom du fichier vidéo
cap = cv2.VideoCapture('C:\\Users\\othman\\Desktop\\ateliers\\vtest.avi')

# Vérifier si la caméra s'est ouverte avec succès
if (cap.isOpened()== False): 
    print("Error opening video stream or file")
    
else:
    while(cap.isOpened()):
        ret, frame = cap.read()
        
        # Casser la boucle
        if ret == False:
            break
        
        img = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        width, height = img.size
        newimg = Image.new("RGB",(width,height),"white")
        filtre = [(0,0)] * 9
        # problem d'analyse 
        for i in range(1,width-1):
            for j in range(1,height-1):
                filtre[0] = img.getpixel((i-1,j-1))
                filtre[1] = img.getpixel((i-1,j))
                filtre[2] = img.getpixel((i-1,j+1))
                filtre[3] = img.getpixel((i,j-1))
                filtre[4] = img.getpixel((i,j))
                filtre[5] = img.getpixel((i,j+1))
                filtre[6] = img.getpixel((i+1,j-1))
                filtre[7] = img.getpixel((i+1,j))
                filtre[8] = img.getpixel((i+1,j+1))
                filtre.sort()
                newimg.putpixel((i,j),(filtre[4]))
                
        framenew = np.asarray(newimg) 
        # Convert RGB to BGR 
        #framenew = framenew[:, :, ::-1].copy() 
        if ret == True:
            # Afficher le cadre obtenu 
            cv2.imshow('Frame',framenew)
            # Appuyez sur e sur le clavier pour quitter
            if cv2.waitKey(25) & 0xFF == ord('e'):
                break
  
   # Lorsque tout est terminé, libérez l'objet de capture vidéo 
    cap.release()
   # Ferme tous les cadres par exemple lorsque clique sur e
    cv2.destroyAllWindows()
    



