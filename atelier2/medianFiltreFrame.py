from PIL import Image
path = "akka.png"
img = Image.open(path)
filtre = [(0,0)] * 9
width, height = img.size
newimg = Image.new("RGB",(width,height),"white")
for i in range(1,width-1):
    for j in range(1,height-1):
        filtre[0] = img.getpixel((i-1,j-1))
        filtre[1] = img.getpixel((i-1,j))
        filtre[2] = img.getpixel((i-1,j+1))
        filtre[3] = img.getpixel((i,j-1))
        filtre[4] = img.getpixel((i,j))
        filtre[5] = img.getpixel((i,j+1))
        filtre[6] = img.getpixel((i+1,j-1))
        filtre[7] = img.getpixel((i+1,j))
        filtre[8] = img.getpixel((i+1,j+1))
        filtre.sort()
        newimg.putpixel((i,j),(filtre[4]))
        
newimg.show('akka1.png')
newimg.show('akka.png')