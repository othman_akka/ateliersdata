import cv2
import numpy as np

#Créer un objet VideoCapture et lire à partir du fichier d'entrée
# Si l'entrée est la caméra, transmettez 0 à la place du nom du fichier vidéo
#capture = cv2.VideoCapture(0)
capture = cv2.VideoCapture('C:\\Users\\othman\\Desktop\\ateliers\\vtest.avi')

ret, frame1 = capture.read()

# extraire taille de frame
width,height,_ = frame1.shape

def listFrameBackgoung(cap):
    #list de frames
    list=[]
    
    #total de frames
    total = int(capture.get(cv2.CAP_PROP_FRAME_COUNT))
    for frameB  in range(0,5):
        ret,frame = cap.read()
        #convertir frame au gris
        frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY) 
        #detruit le bruit
        frame = cv2.GaussianBlur(frame,(5,5),0)
        #ajouter frame en array
        list.append(frame) 
        
    return list;

# Vérifier si la caméra s'est ouverte avec succès
if (capture.isOpened()== False): 
    print("Error opening video stream or file")
else:
    
    #appeler fonction de liste de frames
    list=listFrameBackgoung(capture)
    
    while(capture.isOpened()):
        
        #casser boucle 
        if ret == False:
            break
        else:
            
            #capturer frame par frame
            ret, frame1 = capture.read()
            frame1 = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
            frame1 = cv2.GaussianBlur(frame1,(5,5),0)
            
            # matrice de liste de frames
            matriceFrameBackground=np.array(list)
    
            # background
            background = np.median(matriceFrameBackground, axis=0).reshape(width, height)
            background=background.astype('uint8')
            
            #calculer la difference netre background et frame actuel
            frameDiff = cv2.absdiff(frame1,background)
            _,frameBiniriser = cv2.threshold(frameDiff,20,255,cv2.THRESH_BINARY)
        
            # Afficher le cadre obtenu
            cv2.imshow('Frame_Binary',frameBiniriser)
            cv2.imshow('Arriere plan',background)
            
            # Appuyez sur e sur le clavier pour quitter
            if cv2.waitKey(25) & 0xFF == ord('e'):
                break
            
            #supprimer la premiere frame 
            list.pop(0)
            # ajouter les frames en list
            list.append(frame1)
        
    #Lorsque tout est terminé, libérez l'objet de capture vidéo
    capture.release()
    # Ferme tous les cadres
    cv2.destroyAllWindows()

