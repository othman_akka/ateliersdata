import cv2

#Créer un objet VideoCapture et lire à partir du fichier d'entrée
# Si l'entrée est la caméra, transmettez 0 à la place du nom du fichier vidéo
cap = cv2.VideoCapture('C:\\Users\\othman\\Desktop\\ateliers\\vtest.avi')

#creer deux frames
ret, frame1 = cap.read()
frame2 = frame1

# Vérifier si la caméra s'est ouverte avec succès
if (cap.isOpened()== False): 
    print("Error opening video stream or file")
else:
    while(cap.isOpened()):
        
       #convertir les frames au grais
        frame_gray1 = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
        frame_gray2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)    
        
        #difference entre deux frames et valeur absolue
        frame_diff = cv2.absdiff(frame_gray1,frame_gray2)
        frame_diffB = cv2.absdiff(frame1,frame2)
        
        if ret == True:
            # Afficher le cadre obtenu
            cv2.imshow('frame diff ',frame_diff) 
            cv2.imshow('frame',frame_diffB) 
            #copier frame1 en frame2
            frame2 = frame1.copy()
            # Capturer frame-par-frame
            ret,frame1 = cap.read()
            #casser la boucle
            if ret == False:
                break
            # Appuyez sur e sur le clavier pour quitter
            if cv2.waitKey(1) & 0xFF == ord('e'):
                 break
             #casser boucle
        else:
            break
      
   #Lorsque tout est terminé, libérez l'objet de capture vidéo
    cap.release()
    # Ferme tous les cadres
    cv2.destroyAllWindows()

