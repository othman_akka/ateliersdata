import cv2
import numpy as np
 
#Créer un objet VideoCapture et lire à partir du fichier d'entrée
# Si l'entrée est la caméra, transmettez 0 à la place du nom du fichier vidéo
cap = cv2.VideoCapture('C:\\Users\\othman\\Desktop\\ateliers\\vtest.avi')
 
ret,frame = cap.read()
 
# modifier le type de données
# réglage en virgule flottante 32 bits
avg1 = np.float32(frame)
avg2 = np.float32(frame)

# Vérifier si la caméra s'est ouverte avec succès
if (cap.isOpened()== False): 
    print("Error opening video stream or file")
else:
    while(cap.isOpened()):
        
        # Capturer frame-par-frame
        ret, frame = cap.read()
        
        # Utilisation de la fonction cv2.accumulateWeighted () qui met à jour la moyenne courante
        # relation dst(x,y)=(1 - alpha).dst(x,y)+alpha.src(x,y)  src : image source , dst :L'accumulateur avg1 alpha 0.1
        #cv2.accumulateWeighted(frame,avg1,0.2)
        cv2.accumulateWeighted(frame,avg2,0.01)
        
        # convertir les éléments de la matrice en valeurs absolues et convertir le résultat en 8 bits.
        res1 = cv2.convertScaleAbs(avg1)
        res2 = cv2.convertScaleAbs(avg2)
        
        if ret == True:
            # Afficher le cadre obtenu
            cv2.imshow('Frame',frame)
            cv2.imshow('avg1',res1)
            cv2.imshow('avg2',res2)
            
       # Appuyez sur e sur le clavier pour quitter
            if cv2.waitKey(25) & 0xFF == ord('e'):
                break
        # casser boucle
        else:
            break
        
    #Lorsque tout est terminé, libérez l'objet de capture vidéo
    cap.release()
    # Ferme tous les cadres
    cv2.destroyAllWindows()
