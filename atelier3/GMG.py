import cv2

#Créer un objet VideoCapture et lire à partir du fichier d'entrée
# Si l'entrée est la caméra, transmettez 0 à la place du nom du fichier vidéo
cap = cv2.VideoCapture('C:\\Users\\othman\\Desktop\\ateliers\\vtest.avi')
# creer background GMG
# peut être utilisé pour générer des éléments structurels de différentes formes
kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
back = cv2.bgsegm.createBackgroundSubtractorGMG()

# Vérifier si la caméra s'est ouverte avec succès
if (cap.isOpened()== False): 
    print("Error opening video stream or file")
    
else:
    while(cap.isOpened()):
        # Capturer frame-par-frame
        ret, frame = cap.read()
        #obtenir le masque de premier plan
        back2 = back.apply(frame)
        # le rôle est de séparer les objets et d'éliminer les petites zones (bruit) MORPH_CLOSE MORPH_GRADIENT
        back3 = cv2.morphologyEx(back2, cv2.MORPH_OPEN, kernel)
        if ret == True:
            # Afficher le cadre obtenu
            cv2.imshow('Frame_GMG',back3)
            cv2.imshow('Frame',frame)
       # Appuyez sur e sur le clavier pour quitter
            if cv2.waitKey(25) & 0xFF == ord('e'):
                break
        # Casser la boucle
        else:
            break
    #Lorsque tout est terminé, libérez l'objet de capture vidéo
    cap.release()
    # Ferme tous les cadres
    cv2.destroyAllWindows()