import cv2

#Créer un objet VideoCapture et lire à partir du fichier d'entrée
# Si l'entrée est la caméra, transmettez 0 à la place du nom du fichier vidéo
cap = cv2.VideoCapture('C:\\Users\\othman\\Desktop\\ateliers\\vtest.avi')
#creer background MOG2
back = cv2.createBackgroundSubtractorMOG2()

# Vérifier si la caméra s'est ouverte avec succès
if (cap.isOpened()== False): 
    print("Error opening video stream or file")
    
else:
    while(cap.isOpened()):
        # Capturer frame-par-frame
        ret, frame = cap.read()
        #obtenir le masque de premier plan
        back2 = back.apply(frame)
        if ret == True:
           # Afficher le cadre obtenu
            cv2.imshow('Frame_MOG2',back2)
            cv2.imshow('Frame',frame)
        # Appuyez sur e sur le clavier pour quitter
            if cv2.waitKey(25) & 0xFF == ord('e'):
                break
         # Casser la boucle
        else:
            break
    #Lorsque tout est terminé, libérez l'objet de capture vidéo
    cap.release()
     # Ferme tous les cadres
    cv2.destroyAllWindows()