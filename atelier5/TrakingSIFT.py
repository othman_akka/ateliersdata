import cv2
import numpy as np


#https://pysource.com/2018/06/05/object-tracking-using-homography-opencv-3-4-with-python-3-tutorial-34/

img = cv2.imread('C:\\Users\\othman\\Desktop\\ateliers\\atelier5\\frames\\image40.jpg', cv2.IMREAD_GRAYSCALE) 
cv2.imshow("akka",img)
#cap = cv2.VideoCapture('C:\\Users\\othman\\Desktop\\ateliers\\vtest.avi')
cap = cv2.VideoCapture(0)
# Features
sift = cv2.xfeatures2d.SIFT_create()
kp_image, desc_image = sift.detectAndCompute(img, None)
# Feature matching
index_params = dict(algorithm=0, trees=5)
search_params = dict()
flann = cv2.FlannBasedMatcher(index_params, search_params)
i=1
while True:
    _, frame = cap.read()
    grayframe = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) # trainimage
    kp_grayframe, desc_grayframe = sift.detectAndCompute(grayframe, None)
    matches = flann.knnMatch(desc_image, desc_grayframe, k=2)
    good_points = []
    for m, n in matches:
        if m.distance < 0.6*n.distance:
            good_points.append(m)
    #img3 = cv2.drawMatches(img, kp_image, grayframe, kp_grayframe, good_points, grayframe)
    # Homography
    if len(good_points) > 10:
        query_pts = np.float32([kp_image[m.queryIdx].pt for m in good_points]).reshape(-1, 1, 2)
        train_pts = np.float32([kp_grayframe[m.trainIdx].pt for m in good_points]).reshape(-1, 1, 2)
        matrix, mask = cv2.findHomography(query_pts, train_pts, cv2.RANSAC, 5.0)
        matches_mask = mask.ravel().tolist()
        # Perspective transform
        h, w = img.shape
        pts = np.float32([[0, 0], [0, h], [w, h], [w, 0]]).reshape(-1, 1, 2)
        
        dst = cv2.perspectiveTransform(pts, matrix)
        
        homography = cv2.polylines(frame, [np.int32(dst)], True, (255, 0, 0), 3)
        
        cv2.imshow("Homography", homography)
    else:
        cv2.imshow("Homography", grayframe)
        i=i+1
        print(i)
    #cv2.imshow("Image", img)
    #cv2.imshow("grayFrame", grayframe)
    #cv2.imshow("img3", img3)
    if cv2.waitKey(10)==ord('e'):
        break
cap.release()
cv2.destroyAllWindows()